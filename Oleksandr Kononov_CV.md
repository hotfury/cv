# Oleksandr Kononov
## Lead Software Engineer
 - Email: hotfury0@gmail.com
 - [Linkedin](https://www.linkedin.com/in/oleksandr-kononov-054738127/)

# Achivements
 - Passed [Exam 483: Programming in C#](https://www.credly.com/badges/b386f1f0-81d0-4cc9-a977-02fe33073665)
 - Performed tech talk [Dig into Multitask and Async](https://bit.ly/3cpEboD)
 - Created a project that visualizes threads in .NET [Multitask](https://bit.ly/3qI77gS)
 - Designed architecture for a new application
 - Created a new microservice from scratch. Designed architecture along with documentation and set up CI/CD pipeline with Unit and Integration tests
 - Successfully delivered project
 - Passed Architecture ramp-up program

 # Languages
 - English: upper-intermediate
 - Ukrainian: native

 # Summary 
Lead Software Engineer with 8+ years experience. Advanced in .NET ASP.NET MVC and ASP.NET Core, .NET 5-8 technologies,
Unit testing. Have experience in team leading.

Have experience in CI/CD configuration code review, release preparation, database development, and front-end development. 

Also, were participated in student mentoring and performing interviews for external candidates. Performed training that contains 10 lectures. 

Successfully passed architecture ramp-up.

# Main Interests
 - Climbing
 - Videogames (single-player or couch Co-Op)
 - Tarantino movies and movies in general
 - Psychiatry and Psychology
 - Neural networks

 # Feedbacks
 ## Customer feedback
Oleksandr Kononov - very gifted team lead and architect. Oleksandr demonstrated deep ownership in an extremely complex project involving the lifeblood of our business - subscription billing. He was able to manage architecture, difficult stakeholders, and individual development tasks.

## Customer feedback
Can't even begin to express the thanks and appreciation feel. We just released one of the most difficult projects of my career. I would also like to call out Oleksandr. Wow. This guy is amazing, smart, hard-working, patient, and always with a smile. Also without him, there is no way we would have been successful.

## Performance review
 - Outstanding performance – delivered most of the calculation process in the Project.
 - Constantly reflected on the requirements and chosen technical solutions, suggesting better architecture tactics and this way contributing to the application’s stability. Example: a strategy for handling country groups, changed approach to market re-calculation.
 - Created a great CI/CD pipeline, making the Project the first service to run integration tests against a DB in the pipeline, guarding it against regression and granting developers more confidence to refactor and develop faster.
 - Done a great job mentoring new people joining the project. Pretty sure all devs on the team learned a lot from him, me included.
 - Project scope not fitting the timelines: worked with amazing speed and delivered much more than was expected, often helping teammates along the way. When we tried to mitigate the lack of people by having developers from another team joining, took most of the onboarding and mentoring activities, which wasn’t easy.

# Career Background
## Covent IT
_2023 - Present (Software Engineer)_

Joined to existing team as a developer. The team started the development of a new microservice that calculates marketing proposition for a customer. The project contained a lot of formulas and manipulation of data. My first contribution to a project was to set up a stable CI/CD pipeline with Unit and Integration tests. After that suggested to use Dapper instead of EntityFramework for data retrieval to boost development and application performance. Worked on infrastructure for RabbitMQ between services.
Successfully finished with development and was transferred to a different area.
My next responsibility was to create and set up a new microservice which will work as a proxy for different clients and will talk to internal services.
I've designed documented and implemented a new microservice from scratch with CI/CD, Unit and Integration tests infrastructure, and Authentication/Authorization mechanism.

## SoftServe
 _2017 - 2023 (Software Engineer - Lead Software Engineer)_

Started as a Software Engineer. Participated in a project that implies support of almost 30 multitenant applications. Held tech lead position at the end. During this project performed an internal tech talk [Dig into Multitask and Async](https://bit.ly/3cpEboD) and successfully passed the knowledge evaluation for Senior Software Engineer. 
 
Then took part in a project that required developing API from scratch. Have been holding a tech lead position for a year. Was responsible for project architecture. Performed training for a team from another project that contains 10 lectures. Performed interviews for external candidates. During this project successfully passed the knowledge evaluation for tech lead (but on the second attempt). The project was successfully released within the timelines.
 
Then helped Front End and SQL guys build an API based on .net for reporting. 

After this took part in the project as a tech lead. Was taking part in requirements elicitation meetings. Was writing technical direction for the stories. Was performing planning, task delegation, and release preparation. Was preparing technical suggestions for architects. Was drawing flow and sequence diagrams to represent data flow and user flow.

The next project role also was TechLead. Performed estimation for new functionality and refactoring activities. Was mentoring newcomers. Developed data migration script using Python. Supported Junior developers with their tasks and delegated stories across the team.

Successfully passed architecture rump-up program.

 ## EPAM Systems
 _2015 - 2017 (Trainee - Software Engineer)_

Started as a trainee Successfully passed the interview for Junior Software Engineer after finishing training internal project.

Participated in the internal project for room booking. Participated in internal training.

Took part in power-shell script development for Sitecore templates provisioning for internal needs.

Started first billable project related to migration from WebForms to ASP.NET MVC. In addition, took a mentor role for trainees.
During this time grew to a Software Engineer position.

# National Technical University "Kharkiv Polytechnic Institute"
_2010 - 2016 (Master Computer Programming)_

Learned computer science, microcontroller schemas and programming

Learned the following languages: Pascal, C++, Assembler, Java, PHP, Python.

Implemented the following neural networks using C#: Hebb neural network, Rosenblatt perceptron, Hemming network, discrete Hopfield network, Kohonen network, Bidirectional associative memory

# Tools
## Programming languages
 - C#
 - SQL
 - Python
 - JavaScript
 - TypeScript
 - HTML, CSS
## Frameworks
 - ASP.NET Core, ASP.NET MVC
 - EntityFramework
 - MassTransit
 - Swagger
 - xUnit
 - FakeItEasy, Moq, NSubstitute
 - Specflow
 - WCF
## Libraries
 - Refit
 - Fluent Validation
 - Dapper
 - JQuery
## Databases
 - MS SQL
 - PostgreSQL
 - MongoDB
 - Couchbase
 - MariaDB
## DevOps
 - GitLab CI
 - Jenkins
 - TeamCity
 - Bitbucket CI
 - Azure DevOps
 - Docker
 - K8S
## Clouds
 - Azure
 - AWS
 - Google Cloud
## IDE
 - Visual Studio
 - Rider
 - VS Code
## Operation systems
 - Windows
 - Unix
## Engines
 - Unity